#!/usr/bin/env node
const fs = require('fs')
const path = require('path')
const f = require('active-lodash')
const plist = require('plist')

const DIR = process.argv[2] || '/Volumes/deploy/DSRepository/'

const workflowPath = path.join(DIR, 'Databases', 'Workflows')
const packagePath = path.join(DIR, 'Packages')
const filePath = path.join(DIR, 'Files')
const imagePath = path.join(DIR, 'Masters', 'HFS')

//

const getKeysFromList = (list, key) => f.compact(f.map(list, item => item[key]))
const listOfAllUseds = (list, key) =>
  f.uniq(f.flatten(f.compact(f.map(list, item => item[key]))))

//

const files = fs.readdirSync(filePath)

const packages = f.chain(fs.readdirSync(packagePath))
  .select(filePath => f.endsWith(filePath, 'pkg'))
  .run()

const images = f.chain(fs.readdirSync(imagePath))
  .select(imagePath => f.endsWith(imagePath, '.hfs.dmg'))
  .run()

const workflows = f.chain(fs.readdirSync(workflowPath))
  .map(baseName => path.join(workflowPath, baseName))
  .select(filePath => f.endsWith(filePath, '.plist'))
  .map(filePath => plist.parse(fs.readFileSync(filePath).toString()))
  .map(flow => ({
    title: flow.title,
    packages: getKeysFromList(flow.steps, 'package'),
    files: getKeysFromList(flow.steps, 'file'),
    images: getKeysFromList(flow.steps, 'image')}))
  .run()

const listOfUnusedPackages = f.difference(packages, listOfAllUseds(workflows, 'packages'))
const listOfUnusedFiles = f.difference(files, listOfAllUseds(workflows, 'files'))
const listOfUnusedImages = f.difference(images, listOfAllUseds(workflows, 'images'))

console.log(`
# Found ${listOfUnusedPackages.length} unused packages:
${listOfUnusedPackages.join('\n')}

# Found ${listOfUnusedFiles.length} unused files:
${listOfUnusedFiles.join('\n')}

# Found ${listOfUnusedImages.length} unused images:
${listOfUnusedImages.join('\n')}

`)
