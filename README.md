# `deploystudio-find-obsoletes`

Given a path to a DeployStudio Repo,
returns lists of Files, Packages and Images that are not used in any Workflow.

## install

- install nodejs: <https://nodejs.org>
- `$ npm install --global 'https://gitlab.zhdk.ch/mfa/deploystudio-find-obsoletes/repository/archive.tar.gz?ref=v1.1.0'`

## use

`$ deploystudio-find-obsoletes '/path/to/repo'`
